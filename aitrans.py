"""
利用ai进行翻译
"""

from http import HTTPStatus
import dashscope



class Qwenturbo(object):
    '''翻译'''
    def __init__(self, apikey) -> None:
        dashscope.api_key = apikey

    def __getitem__(self, str):
        response = dashscope.Generation.call(
            model=dashscope.Generation.Models.qwen_max,
            prompt="请翻译：{:s}。不要输出多余内容。".format(str)
        )
        # The response status_code is HTTPStatus.OK indicate success,
        # otherwise indicate request is failed, you can get error code
        # and message from code and message.
        if response.status_code == HTTPStatus.OK:
            return response.output["text"]
            #print(response.output)  # The output text
            #print(response.usage)  # The usage information
        else:
            print(response.code)  # The error code.
            print(response.message)  # The error message
            raise(ConnectionError())
         
